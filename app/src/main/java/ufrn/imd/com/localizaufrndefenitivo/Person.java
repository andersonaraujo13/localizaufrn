package ufrn.imd.com.localizaufrndefenitivo;

/**
 * Created by Ademir on 11/11/2015.
 */
public class Person {

    private double latitude;
    private double longitude;

    public Person(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
