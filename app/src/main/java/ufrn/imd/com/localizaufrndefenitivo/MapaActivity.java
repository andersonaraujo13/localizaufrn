package ufrn.imd.com.localizaufrndefenitivo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.api.client.http.HttpResponse;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapaActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , GoogleApiClient.ConnectionCallbacks ,GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private double DEFAULT_LATITUDE = -31.222222;
    private double DEFAULT_LONGITUDE= 5.222222;
    private int DEFAULT_ZOOM        = 15;
    private int DEFAULT_TILT        = 0;

    private LatLng location = new LatLng(-30.312312, -51.131231);
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;

    private GoogleMap map;
    private Marker marker_location;
    private Marker marker_bus;

    private Person person;

    CameraPosition cameraPosition;
    CameraUpdate update;

    LatLng latLng;

    private Polyline polyline;
    private List<LatLng> list;
    private long distance;

    Context context;

    ArrayList<Bus> buslist;
    private boolean inverso = false;
    private boolean direto = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        GoogleMapOptions options = new GoogleMapOptions();
        options.zOrderOnTop(true);

        person = new Person(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);

        configMap();

        callConnection();



    }

    public void CircularInverso(){
        buslist = new ArrayList<>();
        buslist.add(new Bus(-5.8334344, -35.2029187, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.836580, -35.201926, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.838502, -35.201368, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.8380422,-35.2063577, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.835980, -35.210857, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.842073, -35.2095428, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.8431929,-35.2084631, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.8423398,-35.2065768, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.8438373,-35.2000558, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.844147, -35.197645, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.841592, -35.194984, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.839648,-35.1955018, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.8373623,-35.1971833, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.832608, -35.202793, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.8320141,-35.2043115, "588 - Campus - Inverso"));
        buslist.add(new Bus(-5.832137, -35.204427, "588 - Campus - Inverso"));
        addMarker_bus();

    }
    public void CircularDireto(){
        buslist = new ArrayList<>();
        buslist.add(new Bus(-5.8380422,-35.2063577, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.835980, -35.210857, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.842073, -35.2095428, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.8431929,-35.2084631, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.8423398,-35.2065768, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.8344136,-35.2009081, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.837385, -35.197215, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.839685, -35.195595, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.8440499,-35.1976691, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.8417553,-35.1950467, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.843601, -35.199803, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.8421654,-35.2034017, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.833222, -35.202861, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.8367136, -35.2019452, "588 - Campus - Direto"));
        buslist.add(new Bus(-5.838594, -35.201539, "588 - Campus - Direto"));
        addMarker_bus();
    }
    public void configMap(){

        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setZoomGesturesEnabled(true);

        /* ***************************************** Eventos ************************************** */

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                getRoute(new LatLng(person.getLatitude(), person.getLongitude()), new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mapa, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.Inverso) {
            if(direto == true && inverso == true){
                map.clear();
                CircularInverso();
                direto = false;
            }

            else if(inverso == true && direto == false){
                map.clear();
                inverso = false;
            }else{
                CircularInverso();
                inverso = true;
            }
            return true;
        }
        if (id == R.id.Direto) {
            if(direto == true && inverso == true){
                map.clear();
                CircularDireto();
                inverso = false;
            }
            else if(direto == true && inverso == false)            {
                map.clear();
                direto = false;
            }else{
                CircularDireto();
                direto = true;
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camara) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();

        if(mGoogleApiClient !=null && mGoogleApiClient.isConnected()){
            startLocationUpdate();
        }
    }

    @Override
    public void onPause(){
        super.onPause();

        if(mGoogleApiClient != null){
            stopLocationUpdate();
        }
    }
    private synchronized void callConnection(){
        Log.i("LOG", "callConnection()");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void initLocationRequest(){
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(2000);

        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if(manager.isProviderEnabled( LocationManager.GPS_PROVIDER)){
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        } else {
            Toast.makeText(MapaActivity.this, "GPS está desconectado, conecte ele !", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent, 1);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        }

    }
    private void startLocationUpdate(){
        initLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, MapaActivity.this);
    }

    private void stopLocationUpdate(){
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MapaActivity.this);
    }

    // LISTENER
    @Override
    public void onConnected(Bundle bundle) {
        Log.i("LOG", "onConnected(" + bundle + ")");

        Location l = LocationServices
                .FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if(l != null){
            Log.i("LOG", "latitude: " + l.getLatitude());
            Log.i("LOG", "longitude: " + l.getLongitude());
        }

        startLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("LOG", "onConnectionSuspended(" + i + ")");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("LOG", "onConnectionFailed(" + connectionResult + ")");
    }

    public void camera_update(LatLng latLng){

        cameraPosition = new CameraPosition.Builder().target(latLng).zoom(DEFAULT_ZOOM).build();
        update = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.animateCamera(update);

    }
    public void addMarker_location(LatLng latLng, String tittle, String snippet){

        if(marker_location!=null){
            marker_location.remove();
        }

        marker_location = map.addMarker(new MarkerOptions()
                .position(latLng)
                .title(tittle)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .draggable(true)  );
    }

    public void addMarker_bus(){

        for(Bus key : buslist){
            marker_bus = map.addMarker(new MarkerOptions()
                    .position(new LatLng(key.getLatitude(), key.getLongitude()))
                    .title(key.getLinha())
                    .snippet("")
                    .draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_bus)) );
        }

    }

    @Override
    public void onLocationChanged(Location location) {

        person.setLatitude(location.getLatitude());
        person.setLongitude(location.getLongitude());

        latLng = new LatLng(person.getLatitude(), person.getLongitude());

        camera_update(latLng);
        addMarker_location(latLng, "Sua localização", "Você está localizado aqui!");

    }

    public void drawRoute(){
        PolylineOptions po;

        if(polyline == null){
            po = new PolylineOptions();

            for(int i = 0, tam = list.size(); i < tam; i++){
                po.add(list.get(i));
            }

            po.color(Color.BLACK).width(4);
            polyline = map.addPolyline(po);
        }
        else{
            polyline.setPoints(list);
        }
    }
    /* ***************************************** ROTA ***************************************** */

    // WEB CONNECTION
    public void getRoute(final LatLng origin, final LatLng destination){
        new Thread(){
            public void run(){

                String url= "http://maps.googleapis.com/maps/api/directions/json?origin="
                        + origin.latitude+","+origin.longitude+"&destination="
                        + destination.latitude+","+destination.longitude+"&sensor=false";


                org.apache.http.HttpResponse response;
                HttpGet request;
                AndroidHttpClient client = AndroidHttpClient.newInstance("route");

                request = new HttpGet(url);
                try {
                    response = client.execute(request);
                    final String answer = EntityUtils.toString(response.getEntity());

                    runOnUiThread(new Runnable(){
                        public void run(){
                            try {

                                list = buildJSONRoute(answer);
                                drawRoute();
                            }
                            catch(JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
                catch(IOException e) {
                    e.printStackTrace();
                }

                client.close();

            }

        }.start();
    }

    // PARSER JSON
    public List<LatLng> buildJSONRoute(String json) throws JSONException {
        JSONObject result = new JSONObject(json);
        JSONArray routes = result.getJSONArray("routes");

        distance = routes.getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getInt("value");
        Toast.makeText(MapaActivity.this, "Distance: "+distance+" metros", Toast.LENGTH_LONG).show();

        JSONArray steps = routes.getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONArray("steps");
        List<LatLng> lines = new ArrayList<LatLng>();

        for(int i=0; i < steps.length(); i++) {
            Log.i("Script", "STEP: LAT: "+steps.getJSONObject(i).getJSONObject("start_location").getDouble("lat")+" | LNG: "+steps.getJSONObject(i).getJSONObject("start_location").getDouble("lng"));


            String polyline = steps.getJSONObject(i).getJSONObject("polyline").getString("points");

            for(LatLng p : decodePolyline(polyline)) {
                lines.add(p);
            }

            Log.i("Script", "STEP: LAT: "+steps.getJSONObject(i).getJSONObject("end_location").getDouble("lat")+" | LNG: "+steps.getJSONObject(i).getJSONObject("end_location").getDouble("lng"));
        }

        return(lines);
    }




    // DECODE POLYLINE
    private List<LatLng> decodePolyline(String encoded) {

        List<LatLng> listPoints = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)), (((double) lng / 1E5)));
            Log.i("Script", "POL: LAT: "+p.latitude+" | LNG: "+p.longitude);
            listPoints.add(p);
        }
        return listPoints;
    }
}
