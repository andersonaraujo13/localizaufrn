package ufrn.imd.com.localizaufrndefenitivo;

/**
 * Created by Ademir on 12/11/2015.
 */
public class Bus {

    private double latitude;
    private double longitude;
    private String linha;

    public Bus(double latitude, double longitude, String linha) {
        this.latitude = latitude;
        this.linha = linha;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
